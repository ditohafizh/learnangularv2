import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    feature = 'recipe';

    onNavigate(featureSelected: string) {
        this.feature = featureSelected;
    }
}
