import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
    selector: 'app-recipes-list',
    templateUrl: './recipes-list.component.html',
    styleUrls: ['./recipes-list.component.scss']
})
export class RecipesListComponent implements OnInit {
    recipes: Recipe[] = ([] = [
        new Recipe(
            'A Test Recipe',
            'A Test Recipe Description',
            'https://d1y37rophvf5gr.cloudfront.net/Images/Recipes/recipe-528.700x525.jpg'
        ),
        new Recipe(
            'Another Test Recipe',
            'A Test Recipe Description',
            'https://d1y37rophvf5gr.cloudfront.net/Images/Recipes/recipe-528.700x525.jpg'
        )
    ]);
    @Output() wasRecipeSelected = new EventEmitter<Recipe>();

    constructor() {}

    ngOnInit() {}

    onSelectRecipe(recipe: Recipe) {
        this.wasRecipeSelected.emit(recipe);
    }
}
