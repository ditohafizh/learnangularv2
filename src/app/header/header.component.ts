import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    @Input() activeFeature: string;
    @Output() featureSelected = new EventEmitter<string>();

    onSelectFeature(feature: string) {
        this.featureSelected.emit(feature);
    }
}
